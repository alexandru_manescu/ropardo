import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/facturi")
public class Facturi extends HttpServlet{
	
	DataBaseController dataBase = null;
	
	public void init() throws ServletException
	{
		try {
			dataBase = new DataBaseController();
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	public void doGet(HttpServletRequest request,
			HttpServletResponse response)
						throws ServletException, IOException
	{
		PrintWriter out = response.getWriter();
		
		
		try {
			
			ResultSet rs = dataBase.executeQuery("select NrFactura, DataFactura, CodClient, FacturaRetea, ValoareFactura, ValoareTVA from facturi");
			response.setContentType("text/html");
	  		out.println("<html><body><table>");
	  		while(rs.next()){
				  //Retrieve by column name
	  			  String nrFactura = rs.getString("nrFactura");
	  			  String dataFactura = rs.getString("dataFactura");
	  			  String codClient  = rs.getString("codClient");
		          String facturaRetea = rs.getString("facturaRetea");
		          String valoareFactura = rs.getString("valoareFactura");
		          String valoareTVA = rs.getString("valoareTVA");
		          
		          out.println("<tr><td><form action=\"/Ropardo/facturi\" method=\"post\" target=\"_blank\">");
		          out.println("NrFactura: <input type=\"text\" name=\"nrFactura\" value=\"" + nrFactura + "\"><br>");
			      out.println("DataFactura: <input type=\"text\" name=\"dataFactura\" value=\"" + dataFactura + "\"><br>");
			      out.println("CodClient: <input type=\"text\" name=\"codClient\" value=\"" + codClient + "\"><br>");
			      out.println("FacturaRetea: <input type=\"text\" name=\"facturaRetea\" value=\"" + facturaRetea + "\"><br>");
			      out.println("ValoareFactura: <input type=\"text\" name=\"valoareFactura\" value=\"" + valoareFactura + "\"><br>");
			      out.println("ValoareTVA: <input type=\"text\" name=\"valoareTVA\" value=\"" + valoareTVA + "\"><br>");
			      out.println("<input type=\"submit\" value=\"actualizare\">");
		          //  + codClient + "</form></td></tr>");
				
			
				}
				out.println("</table></html></body>");
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			out.println("<h1>" + "a intervenit o problema" + "</h1>");
			e.printStackTrace();
		}
	
	}
	
	public void doPost(HttpServletRequest request,
			HttpServletResponse response)
						throws ServletException, IOException 
	{
		String nrFactura = request.getParameter("nrFactura");
		String dataFactura = request.getParameter("dataFactura");
		String codClient = request.getParameter("codClient");
		String facturaRetea = request.getParameter("facturaRetea");
		String valoareFactura = request.getParameter("valoareFactura");
		String valoareTVA = request.getParameter("valoareTVA");
 		 		
          PrintWriter out = response.getWriter();
          
          
          
		try {
			dataBase.executeUpdate("update facturi set DataFactura='" + dataFactura + "', CodClient=" + codClient +" , FacturaRetea="+facturaRetea+
					", ValoareFactura=" + valoareFactura + ", ValoareTVA=" + valoareTVA + " where NrFactura=" + nrFactura);
			
	//		dataBase.executeUpdate("update facturi set valoareFactura=" + valoareFactura + "" + " where NrFactura=" + nrFactura);
			
			
			
			
			response.setContentType("text/html");
		    out.println("<h1>" + "factura actualizata" + "</h1>");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			out.println("<h1>" + "a intervenit o problema" + "</h1>");
			e.printStackTrace();
		}

	     
		
		
	}

}
