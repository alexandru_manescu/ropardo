import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/inregistrare-factura")
public class FactureRegistration extends HttpServlet{

	public void init() throws ServletException
	{
	  
	}
	
	public void doGet(HttpServletRequest request,
			HttpServletResponse response)
						throws ServletException, IOException
	{
	      PrintWriter out = response.getWriter();

		out.println("<html><body>");
	      out.println("<form action=\"/Ropardo/inregistrare-factura\" method=\"post\" target=\"_blank\">");
	      out.println("NrFactura: <input type=\"text\" name=\"nrFactura\"><br>");
	      out.println("DataFactura: <input type=\"text\" name=\"dataFactura\"><br>");
	      out.println("CodClient: <input type=\"text\" name=\"codClient\"><br>");
	      out.println("FacturaRetea: <input type=\"text\" name=\"facturaRetea\"><br>");
	      out.println("ValoareFactura: <input type=\"text\" name=\"valoareFactura\"><br>");
	      out.println("ValoareTVA: <input type=\"text\" name=\"valoareTVA\"><br>");
	      out.println("<input type=\"submit\" value=\"inregistrare\">");
	      out.println("</form></html></body>");
	}
	
	public void doPost(HttpServletRequest request,
			HttpServletResponse response)
						throws ServletException, IOException 
	{
	
		String nrFactura = request.getParameter("nrFactura");
		String dataFactura = request.getParameter("dataFactura");
		String codClient = request.getParameter("codClient");
		String facturaRetea = request.getParameter("facturaRetea");
		String valoareFactura = request.getParameter("valoareFactura");
		String valoareTVA = request.getParameter("valoareTVA");
		
		DataBaseController dataBase = null;
		try {
			dataBase = new DataBaseController();
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		PrintWriter out = response.getWriter();
		
		try {
			dataBase.executeUpdate("insert into facturi(NrFactura, DataFactura,"
					+ " CodClient, FacturaRetea, ValoareFactura, "+ "ValoareTVA) "
							+ "values('" + nrFactura + "', '" + dataFactura + "', '"+codClient +"', '"+facturaRetea +"', '" + valoareFactura +"', '" + valoareTVA + "')");
			response.setContentType("text/html");
		    out.println("<h1>" + "factura inregistrata" + "</h1>");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			out.println("<h1>" + "a intervenit o problema" + "</h1>");
			e.printStackTrace();
		}

	     
	}

	
	
}
