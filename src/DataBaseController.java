import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;

public class DataBaseController {
	
	 String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
	 String DB_URL = "jdbc:mysql://localhost/fadse";

	 //  Database credentials
	 String USER = "root";
	 String PASS = "root";
	    
	 Connection conn = null;
	 Statement stmt = null;
	
	public DataBaseController() throws ClassNotFoundException, SQLException {
		
		//STEP 2: Register JDBC driver
	       Class.forName("com.mysql.jdbc.Driver");

	       //STEP 3: Open a connection
	       System.out.println("Connecting to database...");
	       conn = (Connection) DriverManager.getConnection(DB_URL,USER,PASS);

	       //STEP 4: Execute a query
	       System.out.println("Creating statement...");
	       stmt = (Statement) conn.createStatement();
		
	}
	
	
	public ResultSet executeQuery(String sql) throws SQLException {
		   
		// sql = "insert into clienti(NumeClient, AdresaClient) values('clientull', 'adresja')"; 
	       ResultSet rs = stmt.executeQuery(sql);

	       return rs;
	}
	
	public void executeUpdate(String sql) throws SQLException {
		stmt.executeUpdate(sql);
	}
	
   
    
    
    

}
