import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/clienti")
public class Clienti extends HttpServlet{

	DataBaseController dataBase = null;
	public void init() throws ServletException
	{
		try {
			dataBase = new DataBaseController();
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	  
	}
	
	
	public void doGet(HttpServletRequest request,
			HttpServletResponse response)
						throws ServletException, IOException
	{
		
		
		
			try {
				
			ResultSet rs = dataBase.executeQuery("select CodClient, NumeClient, AdresaClient from clienti");
			response.setContentType("text/html");
	  		PrintWriter out = response.getWriter();
	  		out.println("<html><body>");
	  		
	  		
	  		out.println("<table>");
	  		out.println("<tr><td>CodClient</td><td>NumeClient</td><td>AdresaClient</td>");
			while(rs.next()){
			  //Retrieve by column name
	          String numeClient  = rs.getString("numeClient");
	          String adresaClient = rs.getString("adresaClient");
	          String codClient = rs.getString("codClient");
	          out.println("<tr><form action=\"/Ropardo/clienti\" method=\"post\" target=\"_blank\">");
	          out.println("<td><input type=\"text\" name=\"codClient\" value=\"" + codClient + "\"></td>");
	          out.println("<td><input type=\"text\" name=\"numeClient\" value=\"" + numeClient + "\"></td>");
	          out.println("<td><input type=\"text\" name=\"adresaClient\" value=\"" + adresaClient + "\"></td>");
	          out.println("<td><input type=\"submit\" value=\"actualizare\"></td></tr>");
	          
	          
	          //    out.println("<tr><td>" + codClient + "</td><td>" + numeClient + "</td><td>" + adresaClient + "</td></tr>");
			
		
			}
			out.println("</table></html></body>");
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		//    out.println("<h1>" + "saddddddd" + "  " + "</h1>");
	
	}
	
	public void doPost(HttpServletRequest request,
			HttpServletResponse response)
						throws ServletException, IOException 
	{
	
		
		String codClient = request.getParameter("codClient");
		String numeClient = request.getParameter("numeClient");
		String adresaClient = request.getParameter("adresaClient");
		
		DataBaseController dataBase = null;
		try {
			dataBase = new DataBaseController();
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		PrintWriter out = response.getWriter();
		
		try {
			dataBase.executeUpdate("update clienti set CodClient=" + codClient +" , NumeClient='"+numeClient+
					"', AdresaClient='" + adresaClient + "'" + " where CodClient=" + codClient);
			
			response.setContentType("text/html");
		    out.println("<h1>" + "date client actualizate" + "</h1>");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			out.println("<h1>" + "a intervenit o problema" + "</h1>");
			e.printStackTrace();
		}

	     
	}

	
	
	
	
}
